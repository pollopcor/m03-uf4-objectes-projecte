package projecte.Clases;

import java.util.ArrayList;

public abstract class Construccion {

	protected String nombre;
	protected int vida;
	protected int maxVida;
	protected int nivel;
	protected boolean destruido;
	protected ArrayList<Persona> trabajadores;
	protected int numMaxTrabajadores;
	
	// Cojer los recursos de la granja
	protected Recursos aldeaRec = Aldea.getAldea().getRecursos();

	//
	// CONSTRUCTOR
	//

	public Construccion() {
		nombre = "Construccion";
		vida = 100;
		maxVida = vida;
		nivel = 1;
		destruido = false;
		trabajadores = new ArrayList<Persona>();
		numMaxTrabajadores = 50;
	}

	public Construccion(String nombre) {
		this();
		this.nombre = nombre;
		trabajadores = new ArrayList<Persona>();
		numMaxTrabajadores = 50;
	}

	//
	// GETTERS & SETTERS
	//

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		if (vida < 0) {
			vida = 0;
			destruido = true;
		} else if (vida > maxVida)
			vida = maxVida;

		this.vida = vida;
	}

	public void modificarVida(int cantidad) {

		this.vida += cantidad; // Permitira sumar o restar vida

		if (vida < 0) {
			vida = 0;
			destruido = true;
		} else if (vida > maxVida)
			vida = maxVida;
	}

	public ArrayList<Persona> getTrabajadores() {
		return trabajadores;
	}
	
	public int getNumTrabajadores() {
		return trabajadores.size();
	}
	
	public int getNumMaxTrabajadores() {
		return numMaxTrabajadores;
	}
	
	public boolean aņadirTrabajador(Persona trabajador) {
		
		if(trabajadores.size() < numMaxTrabajadores) {
			trabajadores.add(trabajador);
			return true;
		} else
			return false;	
	}
	
	public boolean quitarTrabajador(Persona trabajador) {
		return trabajadores.remove(trabajador);
	}
	
	public boolean getDestruido() {
		return destruido;
	}

	public int getNivel() {
		return nivel;
	}

	
	//
	// METODOS
	//
	
	public abstract boolean reparar(); // Metodo publico para reparar vida

	public abstract boolean mejorar(); // Metodo publico para mejorar de nivel

	
	protected boolean mejorarNivel(Recurso recNecesario) {

		if (recNecesario.getCantidad() >= recursosMejorar()) {
			
			maxVida = 100 + nivel * 20;
			vida = maxVida;
			numMaxTrabajadores += 10;
			nivel++;			
			return true;
		} else
			return false;		
	}

	protected boolean repararVida(Recurso recNecesario) {
		
		if(vida == maxVida)
			return false;
		
		// Si tiene suficientes recursos
		if (recNecesario.getCantidad() >= recursosReparar()) {

			// Quitar recursos usados
			recNecesario.modificarCantidad(recursosReparar() * -1);
			vida = maxVida;
			return true;
		} else
			return false;
	}

	
	//RECURSOS NECESARIOS
	
	protected int recursosMejorar() { // Recursos necesarios para mejorar
		return 15 * nivel;
	}
	
	protected int recursosReparar() { // Recursos necesarios para reparar

		// Vida que falta * 0.20 * nivel
		int recNecesarios = (int) ((maxVida - vida) * 0.20 * nivel);

		// Minimo necesitara 1 de material
		if (recNecesarios == 0) {return 1;}

		return recNecesarios;
	}

	

//	//Si tiene recursos necesarios
//	public boolean tieneRecursos(int recActual, int recNecesarios) {
//		return recActual >= recNecesarios;
//	}

}
