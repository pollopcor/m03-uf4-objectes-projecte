package projecte.Clases;

public class Recurso {

	protected int cantidad;
	protected int maxCantidad;

	//
	// SETTERS & GETTERS
	//
	
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
		
		if(this.cantidad < 0)
			this.cantidad = 0;
		else if (this.cantidad > maxCantidad) {
			this.cantidad = maxCantidad;
		}
	}
	
	public void modificarCantidad(int cantidad) {
		this.cantidad += cantidad;
		if(this.cantidad < 0)
			this.cantidad = 0;
		else if (this.cantidad > maxCantidad) {
			this.cantidad = maxCantidad;
		}
	}
	
	public int getMaxCantidad() {
		return maxCantidad;
	}
	public void setMaxCantidad(int maxCantidad) {
		this.maxCantidad = maxCantidad;
	}
	public void modificarMaxCantidad(int cantidad) {
		this.maxCantidad += cantidad;
	}
	

	
	
}
