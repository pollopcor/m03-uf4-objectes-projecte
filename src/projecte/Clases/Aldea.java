package projecte.Clases;

import projecte.Clases.*;
import projecte.Clases.Construcciones.*;
import projecte.Enums.*;
import projecte.Interficies.*;
import projecte.Otros.Funciones;

import java.util.ArrayList;

public class Aldea { //SINGLETONE

	private static Aldea aldea;
	
	private ArrayList<Persona> Miembros = new ArrayList<Persona>();
	private ArrayList<Persona> Cementerio = new ArrayList<Persona>();
	private Persona jefe;
	private Recursos recursos;
	private ArrayList<Construccion> construcciones = new ArrayList<Construccion>();
	
	//Constructor
	private Aldea() {

		//Generar aldea aleatoria
		
		//Numero de miembros
		int num_miembros = 20 + (int)(Math.random() * 20);
		
		//Generar miembros
		for (int i=0;i<num_miembros;i++)
			Miembros.add(new Persona());
		
		//Crea un jefe		
		this.jefe = new Persona();
		this.jefe.edad = 30 + (int)(Math.random() * 40);
		
		//Crea el inventario de recursos
		recursos = Recursos.getRecursos();
		
		//Generar construcciones basicas (se iran aņadiendo mas con el tiempo)
		construcciones.add(new Maderera());
		construcciones.add(new Granja());
		construcciones.add(new Cantera());
		construcciones.add(new Mina());
		construcciones.add(new Matadero());
		construcciones.add(new Fundicion());
		
		// Distribuir trabajadores a las construcciones
		int j=0;
		// - Por cada persona
		for(Persona pers : Miembros) {

			construcciones.get(j).aņadirTrabajador(pers); // Aņadirlo a una construccion
			j++; //Siguiente construccion
			
			if(j >= construcciones.size()) //Vuelve a la primera construccion si llega al final de la lista
				j=0;
		}
		
	}
	
	
	//
	//GETTERS AND SETTERS
	//

	public static Aldea getAldea() {
		if(aldea == null)
			aldea = new Aldea();	
	
		return aldea;
	}

	public Persona getJefe() {
		return jefe;
	}
	
	public boolean setJefe(Persona pers) {
		
		if(pers == null)
			return false;
		
		if(jefe != null)
			return false;
		
		//Asignar un jefe de entre 30 y 70 aņos
		if(pers.getEstado() != EstadoSalud.Muerto && pers.getEdad() >= 30 && pers.getEdad() <= 65)
			jefe = pers;
		
		return true;
	}
	
	public void aņadirMiembro(Persona pers) {
		if(pers.estado != EstadoSalud.Muerto)
			Miembros.add(pers);
	}
	
	public void quitarMiembro(Persona pers) {
		Miembros.remove(pers);
	}
	
	public boolean quitarMiembroPosicion(int posicion) {
		if(posicion < 0 || posicion >= Miembros.size())
			return false;
		else {
			Miembros.remove(posicion);
			return true;
		}
	}
	
	public ArrayList<Persona> getMiembros() {
		return Miembros;
	}
	
	public ArrayList<Persona> getCementerio() {
		return Cementerio;
	}
	
	public int getNumMiembrosVivos() {
		return Miembros.size();
	}
	
	public int getNumMiembrosMuertos() {
		return Cementerio.size();
	}
	
	public ArrayList<Construccion> getConstrucciones() {
		return construcciones;
	}
	
	public Recursos getRecursos() {
		return recursos;
	}
}
