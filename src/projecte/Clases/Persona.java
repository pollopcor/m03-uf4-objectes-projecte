package projecte.Clases;

import projecte.Clases.*;
import projecte.Enums.*;
import projecte.Interficies.*;
import projecte.Otros.Funciones;

public class Persona {

	protected String nombre;
	protected int edad;
	protected TiposSexo sexo;
	protected EstadoSalud estado;
	protected Persona padre;
	protected Persona madre;
	private static int numPersonas = 0;
	

	//
	// CONSTRUCTORES
	//
	
	public Persona() {
		this.sexo = Funciones.sexoAleatorio();
		this.nombre = Funciones.nombreAleatorio(sexo);
		this.edad = (int)(Math.random() * 80 + 1);
		this.estado = EstadoSalud.Saludable;
		numPersonas++;
	}
	
	public Persona(String nombre) {
		this();
		this.nombre = nombre;
	}
	
	public Persona(int edad) {
		this();
		this.edad = edad;
	}
	
	public Persona(String nombre, int edad) {
		this(nombre);
		this.edad = edad;
	}
	
	public Persona(String nombre, int edad, TiposSexo sexo) {
		this(nombre, edad);
		this.sexo = sexo;
	}
	
	public Persona(String nombre, int edad, Persona padre, Persona madre) {
		this(nombre, edad);
		this.padre = padre;
		this.madre = madre;
	}
	
	//
	// GETTERS AND SETTERS
	//
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public EstadoSalud getEstado() {
		return estado;
	}

	public void setEstado(EstadoSalud estado) {
		this.estado = estado;
	}

	public int getEdad() {
		return edad;
	}
	
}
