package projecte.Clases;

public class Recursos { //SINGLETONE

	private static Recursos recursos;
	
	//Materia prima
	private Recurso arboles;
	private Recurso roca;
	private Recurso minerales;
	private Recurso animales;
	
	//Recursos procesados
	private Recurso madera;
	private Recurso comida;
	private Recurso hierro;
	
	//Constructor vacio, se pondra todo a zero
	private Recursos() {
		
	}

	//
	// SETTER & GETTERS
	//
	
	public static Recursos getRecursos() {
		if(recursos == null)
			recursos = new Recursos();	
	
		return recursos;
	}

	public Recurso getArboles() {
		return arboles;
	}

	public Recurso getRoca() {
		return roca;
	}

	public Recurso getMinerales() {
		return minerales;
	}

	public Recurso getAnimales() {
		return animales;
	}

	public Recurso getMadera() {
		return madera;
	}

	public Recurso getComida() {
		return comida;
	}

	public Recurso getHierro() {
		return hierro;
	}
	
}

