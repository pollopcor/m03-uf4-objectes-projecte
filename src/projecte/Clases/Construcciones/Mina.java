package projecte.Clases.Construcciones;

import projecte.Clases.*;
import projecte.Interficies.*;

public class Mina extends Construccion implements ConstruccionRecursos {

	private static int numMina = 0;

	// Constructores
	public Mina() {
		super();
		nombre = "Mina " + numMina;
		numMina++;
	}

	public Mina(String nombre) {
		super(nombre);
		numMina++;
	}

	//
	// METODOS Y FUNCIONES
	//

	@Override
	public boolean obtenerRecursos() {

		//Por cada trabajador: consume 2 comida, genera 4 piedra
		return ConstruccionRecursos.producirRecursos(getNumTrabajadores(), aldeaRec.getComida(), 2, aldeaRec.getMinerales(), 4);
	}

	@Override
	public boolean reparar() {

		// Reparara la construccion usando el material especificado
		return repararVida(aldeaRec.getMadera());
	}

	@Override
	public boolean mejorar() {

		// Mejorar el nivel de la construccion usando el material especificado
		return mejorarNivel(aldeaRec.getMadera());
	}

}
