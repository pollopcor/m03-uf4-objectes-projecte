package projecte.Clases.Construcciones;

import projecte.Clases.*;
import projecte.Interficies.*;

public class Cantera extends Construccion implements ConstruccionRecursos {

	private static int numCantera = 0;
	
	// Constructores
	public Cantera() {
		super();
		nombre = "Cantera " + numCantera;
		numCantera++;
	}

	public Cantera(String nombre) {
		super(nombre);
		numCantera++;
	}

	//
	// METODOS Y FUNCIONES
	//

	@Override
	public boolean obtenerRecursos() {

		//Por cada trabajador: consume 2 comida, genera 4 piedra
		return ConstruccionRecursos.producirRecursos(getNumTrabajadores(), aldeaRec.getComida(), 2, aldeaRec.getRoca(), 4);
	}

	@Override
	public boolean reparar() {

		//Reparara la construccion usando el material especificado
		return repararVida(aldeaRec.getMadera());
	}

	@Override
	public boolean mejorar() {

		//Mejorar el nivel de la construccion usando el material especificado
		return mejorarNivel(aldeaRec.getMadera());
	}

}

