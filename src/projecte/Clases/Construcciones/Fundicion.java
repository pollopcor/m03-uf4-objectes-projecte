package projecte.Clases.Construcciones;

import projecte.Clases.*;
import projecte.Interficies.*;

public class Fundicion extends Construccion implements ConstruccionFabrica {

	private static int numFundicion = 0;

	// Constructores
	public Fundicion() {
		super();
		nombre = "Fundicion " + numFundicion;
		numFundicion++;
	}

	public Fundicion(String nombre) {
		super(nombre);
		numFundicion++;
	}

	//
	// METODOS Y FUNCIONES
	//

	@Override
	public boolean transformarRecursos() {

		int numRecNecesarios = getNumTrabajadores() * 2;
		
		//Si hay comida suficiente, alimentar para producir
		if (aldeaRec.getComida().getCantidad() > numRecNecesarios) {
			aldeaRec.getComida().modificarCantidad(numRecNecesarios * -1); //Gastar comida
			aldeaRec.getMinerales().modificarCantidad(numRecNecesarios * -1); //Gastar minerales
			aldeaRec.getHierro().modificarCantidad(getNumTrabajadores());	 //Generar hierro (1 por cada 2 de mineral)
			return true;
		} else
			return false;
	}

	@Override
	public boolean reparar() {

		//Reparara la construccion usando el material especificado
		return repararVida(aldeaRec.getMadera());
	}

	@Override
	public boolean mejorar() {

		//Mejorar el nivel de la construccion usando el material especificado
		return mejorarNivel(aldeaRec.getMadera());
	}

}


