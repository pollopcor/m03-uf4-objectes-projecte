package projecte.Clases.Construcciones;

import projecte.Clases.*;
import projecte.Interficies.*;

public class Maderera extends Construccion implements ConstruccionRecursos {

	private static int numMaderera = 0;

	// Constructores
	public Maderera() {
		super();
		nombre = "Maderera " + numMaderera;
		numMaderera++;
	}

	public Maderera(String nombre) {
		super(nombre);
		numMaderera++;
	}

	//
	// METODOS Y FUNCIONES
	//

	@Override
	public boolean obtenerRecursos() {

		// Por cada trabajador: consume 2 comida, genera 4 arboles
		return ConstruccionRecursos.producirRecursos(getNumTrabajadores(), aldeaRec.getComida(), 2,	aldeaRec.getArboles(), 4);
	}

	@Override
	public boolean reparar() {

		// Reparara la construccion usando el material especificado
		return repararVida(aldeaRec.getMadera());
	}

	@Override
	public boolean mejorar() {

		// Mejorar el nivel de la construccion usando el material especificado
		return mejorarNivel(aldeaRec.getMadera());
	}

}
