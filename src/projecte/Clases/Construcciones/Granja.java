package projecte.Clases.Construcciones;

import projecte.Clases.*;
import projecte.Interficies.*;

public class Granja extends Construccion implements ConstruccionRecursos {

	private static int numGranja = 0;
	
	// Constructores
	public Granja() {
		super();
		nombre = "Granja " + numGranja;
		numGranja++;
	}

	public Granja(String nombre) {
		super(nombre);
		numGranja++;
	}

	//
	// METODOS Y FUNCIONES
	//

	@Override
	public boolean obtenerRecursos() {
	
		//Por cada trabajador: consume 2 comida, genera 4 animales
		return ConstruccionRecursos.producirRecursos(getNumTrabajadores(), aldeaRec.getComida(), 2, aldeaRec.getAnimales(), 4);		
	}

	@Override
	public boolean reparar() {

		//Reparara la construccion usando el material especificado
		return repararVida(aldeaRec.getMadera());
	}

	@Override
	public boolean mejorar() {

		//Mejorar el nivel de la construccion usando el material especificado
		return mejorarNivel(aldeaRec.getMadera());
	}
}
