package projecte.Clases.Construcciones;

import projecte.Clases.*;
import projecte.Interficies.*;


public class Matadero extends Construccion implements ConstruccionFabrica {

	private static int numMatadero = 0;

	// Constructores
	public Matadero() {
		super();
		nombre = "Matadero " + numMatadero;
		numMatadero++;
	}

	public Matadero(String nombre) {
		super(nombre);
		numMatadero++;
	}

	//
	// METODOS Y FUNCIONES
	//

	@Override
	public boolean transformarRecursos() {

		int numRecNecesarios = getNumTrabajadores() * 2;
		
		//Si hay comida suficiente, alimentar para producir
		if (aldeaRec.getComida().getCantidad() > numRecNecesarios) {
			aldeaRec.getComida().modificarCantidad(numRecNecesarios * -1); //Gastar comida
			aldeaRec.getAnimales().modificarCantidad(numRecNecesarios * -1); //Gastar animales
			aldeaRec.getComida().modificarCantidad(getNumTrabajadores());	 //Generar comida (7 comida por cada 2 animales)
			return true;
		} else
			return false;
	}

	@Override
	public boolean reparar() {

		//Reparara la construccion usando el material especificado
		return repararVida(aldeaRec.getMadera());
	}

	@Override
	public boolean mejorar() {

		//Mejorar el nivel de la construccion usando el material especificado
		return mejorarNivel(aldeaRec.getMadera());
	}

}


