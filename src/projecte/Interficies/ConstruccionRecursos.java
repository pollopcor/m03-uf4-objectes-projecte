package projecte.Interficies;

import projecte.Clases.*;

public interface ConstruccionRecursos {

	// Generara y obtendra los recursos necessarios para despues fabricar materiales como arboles, rocas, animales, etc
	
	
	// - Nota: Al implementarlo, llamara a "producirRecursos()" con valores que predefiniremos segun la construccion
	public abstract boolean obtenerRecursos();
	
	
	// Permite calcular y producir los recursos
	public static boolean producirRecursos(int trabajadores, Recurso recConsumido, int consPorTrabajador, Recurso recProducido,
			int prodPorTrabajador) {

		if (recConsumido.getCantidad() >= consPorTrabajador * trabajadores) {
			recConsumido.modificarCantidad(consPorTrabajador * trabajadores * -1); 	// Consumir recurso
			recProducido.modificarCantidad(prodPorTrabajador * trabajadores); 		// Producir recursos
			return true;
		}
		return true;
	}

}