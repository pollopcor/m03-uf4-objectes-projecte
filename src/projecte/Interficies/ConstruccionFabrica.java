package projecte.Interficies;

import projecte.Clases.*;

public interface ConstruccionFabrica {

	// Transformara la materia prima en recursos como Arboles => Madera, Animales => Comida, etc

	public abstract boolean transformarRecursos();

	// Permite calcular y producir los recursos
	public static boolean producirRecursos(int trabajadores, Recurso recConsumido, int consPorTrabajador,
			Recurso recProducido, int prodPorTrabajador) {

		if (recConsumido.getCantidad() >= consPorTrabajador * trabajadores) {
			recConsumido.modificarCantidad(consPorTrabajador * trabajadores * -1); // Consumir recurso
			recProducido.modificarCantidad(prodPorTrabajador * trabajadores); // Producir recursos
			return true;
		}
		return true;
	}

}
