package projecte.Otros;

import java.io.IOException; //Para controlar las excepciones de lectura/escritura
import java.nio.file.Files;
import java.nio.file.Paths;

import projecte.Enums.TiposSexo;


public class Funciones {

	public static String[] leerLineasArchivo(String fichero) {
		
		try {
			return Files.readAllLines(Paths.get(fichero)).toArray(new String[0]);
		} catch (IOException ex) {
			return null;
		}
	}
	
	public static String nombreAleatorio(TiposSexo sexo) {
		if(sexo == TiposSexo.Hombre)
			return nombreAleatorioHombre();
		else
			return nombreAleatorioMujer();
	}
	
	public static String nombreAleatorioHombre() {
		return Datos.nombresHombre[(int)(Math.random() * Datos.nombresHombre.length)];
	}
	
	public static String nombreAleatorioMujer() {
		return Datos.nombresMujer[(int)(Math.random() * Datos.nombresMujer.length)];
	}

	public static TiposSexo sexoAleatorio() {
		if((int)(Math.random() * 2) == 0)
			return TiposSexo.Hombre;
		else
			return TiposSexo.Mujer;
	}
	
}


//String[] nombresHombre = Funciones.leerLineasArchivo("nombres_hombre.txt");
//if(nombresHombre == null) {
//	System.err.println("Error: Falta el fichero \"nombres_hombre.txt\"");
//	System.exit(1);
//}
//String[] nombresMujer = Funciones.leerLineasArchivo("nombres_mujer.txt");
//if(nombresMujer == null) {
//	System.err.println("Error: Falta el fichero \"nombres_mujer.txt\"");
//	System.exit(1);
//}
