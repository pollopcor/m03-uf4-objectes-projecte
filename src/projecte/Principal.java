package projecte;

import java.util.Scanner;

import projecte.Clases.*;
import projecte.Enums.*;
import projecte.Interficies.*;

public class Principal {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		
		Aldea aldea = Aldea.getAldea();
		Recursos aldeaRec = Aldea.getAldea().getRecursos();

		// Hacer que pase el tiempo
		for (int dia = 0; dia < 360; dia++) {

			// Producir recursos
			for (Construccion construc : aldea.getConstrucciones()) {

				// Hacer que las construcciones recolecten y transformen materiales/recursos
				if (construc instanceof ConstruccionRecursos) {
					// SI es recolector de recursos
					if (((ConstruccionRecursos) construc).obtenerRecursos()) {
						System.out.println(construc.getNombre() + " ha recolectado materia prima.");
					} else
						System.out.println(construc.getNombre() + " no ha podido recolectar.");

					// Si es transformador de recursos
				} else if (construc instanceof ConstruccionFabrica) {
					if (((ConstruccionFabrica) construc).transformarRecursos()) {
						System.out.println(construc.getNombre() + " ha transformado materia prima.");
					} else
						System.out.println(construc.getNombre() + " no ha transformado materia prima.");
				}

			}

			// Que nazcan bebes
			if ((int) (Math.random() * 5) == 0) {
				for (int i = 0; i < (int) (aldea.getNumMiembrosVivos() * 0.25); i++)
					aldea.aņadirMiembro(new Persona(0));
			}

			// Que fallezca gente
			if ((int) (Math.random() * 6) == 0) {
				for (int i = 0; i < (int) (aldea.getNumMiembrosVivos() * 0.15); i++)
					aldea.quitarMiembroPosicion((int)(Math.random() * aldea.getNumMiembrosVivos()));
			}
			
			System.out.println("========== [Resumen] [Dia " + dia + "] ========");
			System.out.println("= Miembros de la tribu actuales: " + aldea.getNumMiembrosVivos());
			System.out.println("= Arboles: " + aldeaRec.getArboles().getCantidad());
			System.out.println("= Roca: " + aldeaRec.getRoca().getCantidad());
			System.out.println("= Minerales: " + aldeaRec.getMinerales().getCantidad());
			System.out.println("= Animales: " + aldeaRec.getAnimales().getCantidad());
			System.out.println("== Recursos procesados ");
			System.out.println("= Madera: " + aldeaRec.getMadera().getCantidad());
			System.out.println("= Comida: " + aldeaRec.getComida().getCantidad());
			System.out.println("= Hierro: " + aldeaRec.getHierro().getCantidad());
			
			System.out.println("\nEnter para pasar al siguiente dia...");
			scn.nextLine();
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

		}

	}

}
